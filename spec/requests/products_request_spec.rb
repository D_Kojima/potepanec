require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:product) { create(:product) }

  describe "GET potepan/products#show" do
    context "product exists" do
      before do
        get potepan_product_url(product.id)
      end

      it "is successful" do
        expect(response).to have_http_status(:success)
      end

      it "has correct product name" do
        expect(response.body).to include product.name
      end

      it "has correct price" do
        expect(response.body).to include product.price.to_money.format
      end

      it "has correct description" do
        expect(response.body).to include product.description
      end

      it "does not have related products" do
        expect(response.body).not_to include "関連商品"
      end
    end

    context "product does not exist" do
      it "is error" do
        expect { get potepan_product_url 1 } .to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
