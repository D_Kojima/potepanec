require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET potepan/categories#show" do
    describe "category exists" do
      let(:product1)  { create(:product, price: 11.11, taxons: [taxon1]) }
      let(:product2)  { create(:product, price: 22.22, taxons: [taxon2]) }
      let(:taxon1)    { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
      let(:taxon2)    { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
      let(:taxonomy)  { create(:taxonomy) }

      before do
        get potepan_category_url(product1.taxons.find_by(taxonomy_id: taxonomy.id).id)
      end

      it "is successful" do
        expect(response).to have_http_status(:success)
      end

      context "has only product1 name" do
        it { expect(response.body).to     include product1.name }
        it { expect(response.body).not_to include product2.name }
      end

      context "has only product1 price" do
        it { expect(response.body).to     include product1.price.to_money.format }
        it { expect(response.body).not_to include product2.price.to_money.format }
      end
    end

    describe "category does not exist" do
      it "is error" do
        expect { get potepan_category_url 1 } .to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
