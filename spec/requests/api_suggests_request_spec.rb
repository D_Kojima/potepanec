require 'rails_helper'

RSpec.describe "Api Suggests", type: :request do
  let!(:suggest_list1) { create(:potepan_suggest, keyword: "apple") }
  let!(:suggest_list2) { create(:potepan_suggest, keyword: "amazon") }
  let!(:suggest_list3) { create(:potepan_suggest, keyword: "amd") }
  let!(:suggest_list4) { create(:potepan_suggest, keyword: "google") }
  let!(:suggest_list5) { create(:potepan_suggest, keyword: "facebook") }
  let(:headers)        { { Authorization: "Bearer #{api_key}" } }
  let(:query)          { "?keyword=#{keyword}&max_num=#{max_num}" }
  let(:keyword)        { "a" }
  let(:max_num)        { nil }

  describe "Request succeeds with correct key and parameters" do
    let(:api_key) { Rails.application.credentials.potepan_my_suggest[:api_key] }

    before do
      get "/potepan/api/suggests#{query}", headers: headers
    end

    context "keyword is 'a'" do
      let(:a_words) { [suggest_list1.keyword, suggest_list2.keyword, suggest_list3.keyword] }

      shared_examples "Three words with the initial letter 'a' are returned" do
        it "returns all words starting with 'a' and doesn't return anything else" do
          expect(response.body).to eq a_words.to_json
        end
      end

      it "returns status code 200" do
        expect(response).to have_http_status(200)
      end

      context "max_num is nil" do
        it_behaves_like "Three words with the initial letter 'a' are returned"
      end

      context "max_num is a number less than 1" do
        let(:max_num) { 0 }

        it_behaves_like "Three words with the initial letter 'a' are returned"
      end

      context "max_num is not number" do
        let(:max_num) { "hoge" }

        it_behaves_like "Three words with the initial letter 'a' are returned"
      end

      context "max_num is 2" do
        let(:max_num) { 2 }

        it "returns only 2 words" do
          expect(JSON.parse(response.body).length).to eq max_num
        end

        it "only returns words starting with 'a'" do
          expect(JSON.parse(response.body)).to all(satisfy { |keyword| a_words.include?(keyword) })
        end
      end
    end
  end

  describe "Wrong api key" do
    let(:api_key) { "hoge" }

    before do
      get "/potepan/api/suggests#{query}", headers: headers
    end

    it "returns status code 401" do
      expect(response).to have_http_status(401)
    end

    it "returns the error message of unauthorized" do
      expect(response.body).to eq({ error: { message: "unauthorized" } }.to_json)
    end
  end

  describe "Keyword is nil" do
    let(:api_key) { Rails.application.credentials.potepan_my_suggest[:api_key] }

    before do
      get "/potepan/api/suggests", headers: headers
    end

    it "returns status code 400" do
      expect(response).to have_http_status(400)
    end

    it "returns the error message of no keyword" do
      expect(response.body).to eq({ error: { message: "keyword can't be nil." } }.to_json)
    end
  end
end
