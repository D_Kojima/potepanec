require 'rails_helper'
require 'webmock/rspec'

RSpec.describe "Suggests", type: :request do
  describe "topbar search suggest" do
    let(:url)     { Rails.application.credentials.potepan_suggest[:url] }
    let(:api_key) { Rails.application.credentials.potepan_suggest[:api_key] }

    before do
      WebMock.enable!
      WebMock.stub_request(:get, url).
        with(
          headers: { Authorization: "Bearer #{api_key}" },
          query: { keyword: keyword, max_num: SUGGEST_MAX_COUNT }
        ).
        to_return(
          body: body,
          status: status
        )
      get potepan_suggests_path, params: { keyword: keyword, max_num: SUGGEST_MAX_COUNT }
    end

    context "When 'a' is entered in the search form" do
      let(:keyword) { "a" }
      let(:body)    { ["apache", "apache for women", "apache for men"].to_json }
      let(:status)  { 200 }

      it "returns status code 200" do
        expect(response).to have_http_status(200)
      end

      it "suggest 3 products starting with 'a'" do
        expect(response.body).to eq body
      end
    end

    context "When requesting with keyword nil" do
      let(:keyword) { nil }
      let(:body)    { "unauthorized".to_json }
      let(:status)  { 200 }

      it "returns status code 400" do
        expect(response).to have_http_status(400)
      end

      it "returns empty array" do
        expect(JSON.parse(response.body)).to eq []
      end
    end

    context "The keyword is correct, but for some reason response status is not 200" do
      let(:keyword) { "valid keyword" }
      let(:body)    { ["valid keyword"].to_json }
      let(:status)  { 401 }

      it "returns status code 401" do
        expect(response).to have_http_status(401)
      end

      it "returns empty array" do
        expect(JSON.parse(response.body)).to eq []
      end
    end
  end
end
