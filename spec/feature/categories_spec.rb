require 'rails_helper'

RSpec.describe 'Categories', type: :feature do
  include ApplicationHelper
  feature "potepan/category#show" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon)    { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:taxonomy) { create(:taxonomy, id: 1) }

    feature "General elements" do
      context "Category page specific elements" do
        before do
          visit potepan_category_path(taxon.id)
        end

        scenario "Page title is '(taxon.name) - BIGBAG Store'" do
          expect(page).to have_title(full_title(page_title: taxon.name))
        end

        scenario "PageHeader has shop link" do
          within ".pageHeader" do
            expect(page).to have_link "shop"
          end
        end

        scenario "Two category names are written in pageheader" do
          title_count = within(".pageHeader") { all(".page-title") }
          expect(title_count.size).to eq 2
        end

        scenario "ProductBox has product page link" do
          # 商品欄
          within(".productsArea") { find(".product-#{product.id}").click }
          expect(current_path).to eq potepan_product_path(product.id)
        end
      end
    end

    feature "Categories side bar" do
      before do
        visit potepan_category_path(taxon.id)
      end

      scenario "Category panel has taxonomy name" do
        within ".sideBar" do
          expect(page).to have_selector ".panel-category", text: taxonomy.root.name
        end
      end

      scenario "There is leaf element under the taxonomy" do
        expect(page).to have_selector "#taxonomy-#{taxonomy.id}", text: taxon.name
      end

      scenario "Number of items in category is one" do
        expect(page).to have_selector "#taxonomy-#{taxonomy.id}", text: "(1)"
      end
    end
  end
end
