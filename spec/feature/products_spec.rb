require 'rails_helper'

RSpec.describe 'Products', type: :feature do
  include ApplicationHelper
  feature "potepan/product#show" do
    feature "General elements" do
      let(:product) { create(:product) }

      context "Common template" do
        before do
          visit potepan_product_path(product.id)
        end

        scenario "Header logo has index link" do
          # BIGBAGロゴ
          within(".navbar-header") { find('.navbar-brand').click }
          expect(current_path).to eq potepan_index_path
        end

        scenario "Navbar Home has index link" do
          within(".navbar") { click_link "Home" }
          expect(current_path).to eq potepan_index_path
        end

        scenario "PageHeader Home has index link" do
          within(".pageHeader") { click_link "Home" }
          expect(current_path).to eq potepan_index_path
        end
      end

      context "Product page specific elements" do
        before do
          visit potepan_product_path(product.id)
        end

        scenario "Page title is '(product.name) - BIGBAG Store'" do
          expect(page).to have_title(full_title(page_title: product.name))
        end

        scenario "PageHeader does not have shop link" do
          within ".pageHeader" do
            expect(page).not_to have_link "shop"
          end
        end

        scenario "Two product names are written in pageheader" do
          title_count = within(".pageHeader") { all(".page-title") }
          expect(title_count.size).to eq 2
        end
      end
    end

    feature "Back to the categories page link" do
      let(:taxon1)    { create(:taxon, taxonomy: taxonomy1, parent_id: taxonomy1.root.id) }
      let(:taxon2)    { create(:taxon, taxonomy: taxonomy2, parent_id: taxonomy2.root.id) }
      let(:taxon3)    { create(:taxon, taxonomy: taxonomy2, parent_id: taxonomy2.root.id) }
      let(:taxonomy1) { create(:taxonomy, name: "Categories") }
      let(:taxonomy2) { create(:taxonomy, name: "Brand") }

      context "Product with 'Categories' taxonomy" do
        let(:product) { create(:product, taxons: [taxon1, taxon2]) }

        before do
          visit potepan_product_path(product.id)
        end

        scenario "Link to the taxon show page (taxonomy of taxon is 'Categories')" do
          within(".media-body") { click_link "一覧ページへ戻る" }
          expect(current_path).to eq potepan_category_path(taxon1.id)
        end
      end

      context "Products with taxonomy other than 'Categories'" do
        let(:product) { create(:product, taxons: [taxon2]) }

        before do
          visit potepan_product_path(product.id)
        end

        scenario "Link to the taxon show page (taxonomy of taxon is not 'Categories')" do
          within(".media-body") { click_link "一覧ページへ戻る" }
          expect(current_path).to eq potepan_category_path(taxon2.id)
        end
      end

      context "Product without taxonomy" do
        let(:product) { create(:product, taxons: []) }

        before do
          visit potepan_product_path(product.id)
        end

        scenario "There is no link to the category page" do
          expect(page).not_to have_link "一覧ページへ戻る"
        end
      end
    end

    feature "Related Products" do
      let(:product) { create(:product, taxons: [taxon1, taxon2]) }
      let(:taxon1)    { create(:taxon, taxonomy: taxonomy1, parent_id: taxonomy1.root.id) }
      let(:taxon2)    { create(:taxon, taxonomy: taxonomy2, parent_id: taxonomy2.root.id) }
      let(:taxon3)    { create(:taxon, taxonomy: taxonomy2, parent_id: taxonomy2.root.id) }
      let(:taxonomy1) { create(:taxonomy, name: "Categories") }
      let(:taxonomy2) { create(:taxonomy, name: "Brand") }

      context "1 to #{NUM_OF_RELATED_PRODUCTS} related products (2 related and 1 unrelated)" do
        let!(:other_product1) { create(:product, taxons: [taxon1]) }
        let!(:other_product2) { create(:product, taxons: [taxon2]) }
        let!(:other_product3) { create(:product, taxons: [taxon3]) }

        before do
          visit potepan_product_path(product.id)
        end

        scenario "2 related products are displayed" do
          product_count = within(".productsContent") { all(".productBox") }
          expect(product_count.size).to eq 2
        end

        scenario "Other product1 (has taxon1) is displayed in the related product" do
          within(".productsContent") do
            expect(page).to have_content other_product1.name
          end
        end

        scenario "Related product1 has product page link" do
          # 関連商品欄 other_product1
          within(".productsContent") { find(".product-#{other_product1.id}").click }
          expect(current_path).to eq potepan_product_path(other_product1.id)
        end

        scenario "Other product2 (has taxon2) is displayed in the related product" do
          within(".productsContent") do
            expect(page).to have_content other_product2.name
          end
        end

        scenario "Related product2 has product page link" do
          # 関連商品欄 other_product2
          within(".productsContent") { find(".product-#{other_product2.id}").click }
          expect(current_path).to eq potepan_product_path(other_product2.id)
        end

        scenario "Other product3 (doesn't have taxon1,2) is not displayed in the related product" do
          within(".productsContent") do
            expect(page).not_to have_content other_product3.name
          end
        end
      end

      context "More than #{NUM_OF_RELATED_PRODUCTS} related products" do
        let!(:other_product1) { create(:product, taxons: [taxon1]) }
        let!(:other_product2) { create(:product, taxons: [taxon1]) }
        let!(:other_product3) { create(:product, taxons: [taxon1]) }
        let!(:other_product4) { create(:product, taxons: [taxon1]) }
        let!(:other_product5) { create(:product, taxons: [taxon1]) }

        before do
          visit potepan_product_path(product.id)
        end

        scenario "#{NUM_OF_RELATED_PRODUCTS} related products are displayed" do
          product_count = within(".productsContent") { all(".productBox") }
          expect(product_count.size).to eq NUM_OF_RELATED_PRODUCTS
        end
      end
    end
  end
end
