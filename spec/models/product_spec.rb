require 'rails_helper'

RSpec.describe "Product", type: :model do
  describe "Instance method" do
    describe "related_products" do
      subject { product.related_products }

      let(:product)   { create(:product, taxons: [taxon1, taxon3]) }
      let(:taxon1)    { create(:taxon, taxonomy: taxonomy1, parent_id: taxonomy1.root.id) }
      let(:taxon2)    { create(:taxon, taxonomy: taxonomy1, parent_id: taxonomy1.root.id) }
      let(:taxon3)    { create(:taxon, taxonomy: taxonomy2, parent_id: taxonomy2.root.id) }
      let(:taxon4)    { create(:taxon, taxonomy: taxonomy2, parent_id: taxonomy2.root.id) }
      let(:taxonomy1) { create(:taxonomy) }
      let(:taxonomy2) { create(:taxonomy) }

      context "other_product has 2 taxons, 2 of which matches" do
        let!(:other_product) { create(:product, taxons: [taxon1, taxon3]) }

        it { is_expected.to eq [other_product] }
      end

      context "other_product has 2 taxons, 1 of which matches" do
        let!(:other_product) { create(:product, taxons: [taxon1, taxon4]) }

        it { is_expected.to eq [other_product] }
      end

      context "other_product has 2 taxons, but no match" do
        let!(:other_product) { create(:product, taxons: [taxon2, taxon4]) }

        it { is_expected.to eq [] }
      end

      context "other_product has 1 taxon, 1 of which matches" do
        let!(:other_product) { create(:product, taxons: [taxon1]) }

        it { is_expected.to eq [other_product] }
      end

      context "other_product has 1 taxon, but no match" do
        let!(:other_product) { create(:product, taxons: [taxon2]) }

        it { is_expected.to eq [] }
      end

      context "other_product has no taxon" do
        let!(:other_product) { create(:product, taxons: []) }

        it { is_expected.to eq [] }
      end
    end
  end
end
