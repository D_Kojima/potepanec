require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  describe "full_title method" do
    subject { full_title(page_title: target) }

    describe "argument is blank" do
      context "no argument" do
        it { expect(full_title).to eq(STORE_NAME) }
      end

      context "nil" do
        let(:target) { nil }

        it { is_expected.to eq(STORE_NAME) }
      end

      context "empty string" do
        let(:target) { "" }

        it { is_expected.to eq(STORE_NAME) }
      end

      context "half-width space" do
        let(:target) { " " }

        it { is_expected.to eq(STORE_NAME) }
      end

      context "full-width space" do
        let(:target) { "　" }

        it { is_expected.to eq(STORE_NAME) }
      end
    end

    describe "argument is not blank" do
      context "normal string" do
        let(:target) { "TEST" }

        it { is_expected.to eq("TEST - #{STORE_NAME}") }
      end

      context "numerical value" do
        let(:target) { 12.34 }

        it { is_expected.to eq("12.34 - #{STORE_NAME}") }
      end

      context "boolean" do
        let(:target) { true }

        it { is_expected.to eq("true - #{STORE_NAME}") }
      end
    end
  end
end
