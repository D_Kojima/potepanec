(function(global) {
  const $suggest_box = $('#ui-id-1');
  const $search_form = $('.form-control');
  const $search_box  = $('.searchBox');

  const display_suggest_dropdown = function(){
    $suggest_box.css({
      'display': 'block',
      'width':   $search_form.css('width'),
      'top':     $search_form.offset().top + parseInt($search_form.css('height')),
      'left':    $search_form.offset().left
    });
  };

  $search_form.on('input', function(){
    const keyword = $search_form.val();
    if (keyword) {
      $.get({
        url:  "/potepan/suggests",
        data: { keyword: keyword }
      }).done(function(suggest_list){
        let menu_items = "";
        suggest_list.forEach(function(suggest_item, i){
          menu_items += '<li class="ui-menu-item" id="ui-id-' + (parseInt(i) + 2) + '" tabindex="-1">' + suggest_item + '</li>';
        });
        $suggest_box.empty().append(menu_items);
        display_suggest_dropdown();
      });
    } else {
      $suggest_box.hide();
    }
  });

  $(document).on('click', '.ui-menu-item', function(){
    $search_form.val($(this).text());
    $suggest_box.hide();
    return false;
  });

  $(document).on('mouseenter', '.ui-menu-item', function(){
    $(this).addClass('ui-state-focus');
  });

  $(document).on('mouseleave', '.ui-menu-item', function(){
    $(this).removeClass('ui-state-focus');
  });

  $suggest_box.hover(
    function(){
      $search_box.addClass('open');
    },
    function(){
      $suggest_box.hide();
    }
  );

  $search_box.on('mouseleave', function(){
    let is_suggest_hover = false;
    $(":hover").each(function(){
      is_suggest_hover = is_suggest_hover || ($(this).attr("id") == "ui-id-1");
    });
    if(!is_suggest_hover) $suggest_box.hide();
  });
})(this);
