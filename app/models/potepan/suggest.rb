class Potepan::Suggest < ActiveRecord::Base
  scope :items_start_with_keyword, ->(keyword) { where("keyword LIKE ?", "#{keyword}%") }
end
