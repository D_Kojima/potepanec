module Potepan::ProductDecorator
  def related_products
    Spree::Product.includes(master: [:images, :default_price]).
      joins(:classifications).
      merge(Spree::Classification.where(taxon_id: taxons.ids)).
      where.not(id: id).
      distinct.
      sample(NUM_OF_RELATED_PRODUCTS)
  end

  Spree::Product.prepend self
end
