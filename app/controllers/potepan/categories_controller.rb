class Potepan::CategoriesController < ApplicationController
  def show
    @category = Spree::Taxon.find(params[:id])
    @products = @category.products.includes(master: [:images, :default_price])
    @major_categories = Spree::Taxonomy.all.includes(:root)
    @minor_categories = []
    @major_categories.each do |major_category|
      @minor_categories[major_category.id] = major_category.root.leaves.includes(:products)
    end
  end
end
