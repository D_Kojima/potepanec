class Potepan::Api::SuggestsController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods
  before_action :authenticate

  def index
    if params[:keyword].nil?
      render_bad_request
      return
    end

    render json: suggest_list.pluck(:keyword)
  end

  private

  def suggest_list
    if params[:max_num].to_i.positive?
      output_count = [params[:max_num].to_i, SUGGEST_MAX_NUM_LIMIT].min
    else
      output_count = SUGGEST_MAX_NUM_LIMIT
    end
    Potepan::Suggest.items_start_with_keyword(params[:keyword]).limit(output_count)
  end

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      token == Rails.application.credentials.potepan_my_suggest[:api_key]
    end
  end

  def render_unauthorized
    render json: { error: { message: "unauthorized" } }, status: :unauthorized
  end

  def render_bad_request
    render json: { error: { message: "keyword can't be nil." } }, status: :bad_request
  end
end
