require "httpclient"
class Potepan::SuggestsController < ApplicationController
  def index
    if params[:keyword].blank?
      logging_error("'keyword' is blank.")
      render status: 400, json: []
      return
    end

    response = Potepan::Suggests.client.get(
      Potepan::Suggests.url,
      Potepan::Suggests.query(keyword: params[:keyword]),
      Potepan::Suggests.header
    )
    if response.status != 200
      logging_error("Suggest request failed. ErrorCode: #{response.status}")
      render status: response.status, json: []
      return
    end

    suggests = JSON.parse(response.body)
    render json: suggests
  end

  private

  def logging_error(message)
    logger.error "Error!"
    logger.error "  message: #{message}"
  end
end
