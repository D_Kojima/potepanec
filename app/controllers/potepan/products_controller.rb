class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxons.joins(:taxonomy).merge(Spree::Taxonomy.where(name: "Categories")).first
    @taxon ||= @product.taxons.first
    @related_products = @product.related_products
  end
end
