module ApplicationHelper
  def full_title(page_title: "")
    if page_title.blank?
      STORE_NAME
    else
      "#{page_title} - #{STORE_NAME}"
    end
  end
end
