class Potepan::Suggests
  class << self
    def url
      Rails.application.credentials.potepan_suggest[:url]
    end

    def query(keyword: "")
      { keyword: keyword, max_num: SUGGEST_MAX_COUNT }
    end

    def header
      { Authorization: "Bearer #{Rails.application.credentials.potepan_suggest[:api_key]}" }
    end

    def client
      HTTPClient.new
    end
  end
end
